![version](https://img.shields.io/badge/version-Beta-black)
# Freefy Search

That's so simple, just type what you want to play and we'll do the rest 🥳

This search engine is powered by [Freefy](https://app.freefy.online).

## Content

	- index.html
	- assets
		- css
			- freefy-search.css
		- images
			- og-image.png
		- js

## Colaborate

Help us 🙏 to maintain this free service  active  [    ![Donate using Liberapay](https://liberapay.com/assets/widgets/donate.svg)
](https://liberapay.com/freefy/donate)

## Copyright

> © 2021 [Freefy](https://app.freefy.online/). Any doubts?
> [hola@freefy.online](mailto:hola@freefy.online "Say Hi or tell us your
> doubts about our free service").

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0MjIyNjU0NjEsLTk2ODAzMTczOSw2Nz
IzOTc1NDFdfQ==
-->